<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.somtijds.nl
 * @since      0.1.0
 *
 * @package    Amstelodamum_Archive
 * @subpackage Amstelodamum_Archive/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.1.0
 * @package    Amstelodamum_Archive
 * @subpackage Amstelodamum_Archive/includes
 * @author     Willem Prins <willem@somtijds.nl>
 */
class Amstelodamum_Archive_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.0
	 */
	public static function activate() {

	}

}
