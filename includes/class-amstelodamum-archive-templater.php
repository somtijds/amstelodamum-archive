<?php
/**
 * Define the template functionality
 *
 * @link       http://www.somtijds.nl
 * @since      0.1.0
 *
 * @package    Amstelodamum_Archive
 * @subpackage Amstelodamum_Archive/includes
 */

class Amstelodamum_Archive_Templater {

	public function add_filters() {

		$this->templates = array();

		// Add a filter to the page attributes metabox to inject our template into the page template cache.
		add_filter('page_attributes_dropdown_pages_args', array( $this, 'register_archive_templates' ) );

		// Add a filter to the save post in order to inject out template into the page cache
		add_filter('wp_insert_post_data', array( $this, 'register_archive_templates' ) );

		// Add a filter to the template include in order to determine if the page has our template assigned and return it's path
		add_filter('template_include', array( $this, 'view_archive_template') );

		// Add your templates to this array.
		$this->templates = array(
			'page-repository-overview.php' => __( 'Archief: overzichtsweergave', 'amstelodamum-archive' ),
			'page-repository-single.php' => __( 'Archief: enkele weergave', 'amstelodamum-archive' ),
			'page-repository-search.php' => __( 'Archief: zoeken en vinden', 'amstelodamum-archive' ),
		);

		// adding support for theme templates to be merged and shown in dropdown
		$templates = wp_get_theme()->get_page_templates();
		$templates = array_merge( $templates, $this->templates );

	} // end filter function

	/**
	 * Adds our template to the pages cache in order to trick WordPress
	 * into thinking the template file exists where it doesn't really exist.
	 *
	 * @param   array    $atts    The attributes for the page attributes dropdown
	 * @return  array    $atts    The attributes for the page attributes dropdown
	 * @verison	0.1.0
	 * @since	0.1.0
	 */
	public function register_archive_templates( $atts ) {

		// Create the key used for the themes cache
		$cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

		// Retrieve the cache list. If it doesn't exist, or it's empty prepare an array
		$templates = wp_cache_get( $cache_key, 'themes' );
		if ( empty( $templates ) ) {
			$templates = array();
		} // end if

		// Since we've updated the cache, we need to delete the old cache
		wp_cache_delete( $cache_key , 'themes');

		// Now add our template to the list of templates by merging our templates
		// with the existing templates array from the cache.
		$templates = array_merge( $templates, $this->templates );

		// Add the modified cache to allow WordPress to pick it up for listing
		// available templates
		wp_cache_add( $cache_key, $templates, 'themes', 1800 );

		return $atts;

	} // end register_archive_templates

	/**
	 * Checks if the template is assigned to the page
	 *
	 * @version	0.1.0
	 * @since	0.1.0
	 */
	public function view_archive_template( $template ) {

		global $post;

		// If no posts found, return to
		// avoid "Trying to get property of non-object" error
		if ( !isset( $post ) ) return $template;

		if ( ! isset( $this->templates[ get_post_meta( $post->ID, '_wp_page_template', true ) ] ) ) {
			return $template;
		} // end if

		$file = dirname( plugin_dir_path( __FILE__ )) . '/templates/' . get_post_meta( $post->ID, '_wp_page_template', true );

		// Just to be safe, we check if the file exist first
		if( file_exists( $file ) ) {
			return $file;
		} // end if

		return $template;

	} // end view_archive_template


	/*--------------------------------------------*
	 * deactivate the plugin
	*---------------------------------------------*/
	static function deactivate( $network_wide ) {
		foreach($this as $value) {
			page-template-example::delete_template( $value );
		}

	} // end deactivate

	/*--------------------------------------------*
	 * Delete Templates from Theme
	*---------------------------------------------*/
	public function delete_template( $filename ){
		$theme_path = get_template_directory();
		$template_path = $theme_path . '/' . $filename;
		if( file_exists( $template_path ) ) {
			unlink( $template_path );
		}

		// we should probably delete the old cache
		wp_cache_delete( $cache_key , 'themes');
	}


} // end class
