<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.somtijds.nl
 * @since      0.1.0
 *
 * @package    Amstelodamum_Archive
 * @subpackage Amstelodamum_Archive/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.1.0
 * @package    Amstelodamum_Archive
 * @subpackage Amstelodamum_Archive/includes
 * @author     Willem Prins <willem@somtijds.nl>
 */
class Amstelodamum_Archive_Deactivator {

	/**
	 * Deactivation hooks.
	 *
	 * Templates will be removed from the templater class
	 *
	 * @since    0.1.0
	 */
	public static function deactivate() {

	}

}
