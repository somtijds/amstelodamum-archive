<?php
/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.somtijds.nl
 * @since      0.1.0
 *
 * @package    Amstelodamum_Archive
 * @subpackage Amstelodamum_Archive/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.1.0
 * @package    Amstelodamum_Archive
 * @subpackage Amstelodamum_Archive/includes
 * @author     Willem Prins <willem@somtijds.nl>
 */
class Amstelodamum_Archive_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.1.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'amstelodamum-archive',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);
	}
}
