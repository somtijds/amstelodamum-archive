<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.somtijds.nl
 * @since      0.1.0
 *
 * @package    Amstelodamum_Archive
 * @subpackage Amstelodamum_Archive/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
