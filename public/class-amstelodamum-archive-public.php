<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.somtijds.nl
 * @since      0.1.0
 *
 * @package    Amstelodamum_Archive
 * @subpackage Amstelodamum_Archive/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Amstelodamum_Archive
 * @subpackage Amstelodamum_Archive/public
 * @author     Willem Prins <willem@somtijds.nl>
 */
class Amstelodamum_Archive_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The option name of this plugin.
	 *
	 * @since    0.2.0
	 * @access   private
	 * @var      string    $option_name    The current version of this plugin.
	 *
	 * @TODO make this into an options / settings array
	 */
	private $option_name;

	/**
	 * The status of the repo.
	 *
	 * @since    0.2.0
	 * @access   public
	 * @var      string    $repo_status    Status to be collected from repo-server.
	 */
	private $repo_status;

	/**
	 * The baseurl for the repo
	 *
	 * @since    0.2.0
	 * @access   private
	 * @var      string    $baseurl  Baseurls for files on repo-server.
	 */
	private $baseurl;

	/**
	 * The number of items to be displayed per page
	 *
	 * @since    0.2.0
	 * @access   private
	 * @var      integer   $items_per_page    The number of items to be displayed per page.
	 */
	private $items_per_page = 50;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.0
	 *
	 * @param string $plugin_name The name of the plugin.
	 * @param string $version The version of this plugin.
	 * @param string $option_name The options for this plugin.
	 */
	public function __construct( $plugin_name, $version, $option_name ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->option_name = $option_name;
		$this->baseurl = trailingslashit( get_option( $this->option_name .'_repo_url' ) );

		$items_per_page = get_option( $this->option_name .'_items_per_page' );
		if ( isset( $items_per_page ) && is_numeric( $items_per_page ) && $items_per_page <= 100 ) {
			$this->items_per_page = $items_per_page;
		}

		$this->init();
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_styles() {

		/**
		 * An instance of this class should be passed to the run() function
		 * defined in Amstelodamum_Archive_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Amstelodamum_Archive_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/' . $this->plugin_name . '-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_scripts() {

		/**
		 * An instance of this class should be passed to the run() function
		 * defined in Amstelodamum_Archive_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Amstelodamum_Archive_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 *
		 * @TODO use plugin options value
		 */
		global $post;
		if ( ! $post instanceof \WP_Post || empty( $post->post_content )) {
			return;
		}
		if (\has_shortcode($post->post_content,'amstelodamum_archive_search')) {
			wp_enqueue_script( $this->plugin_name . '-public-js', plugin_dir_url( __FILE__ ) . 'js/' . $this->plugin_name . '-public.js', array( 'jquery' ), $this->version, false );
		}
	}

	/**
	 * Make variables available for javascript
	 *
	 * @since    0.2.0
	 */
	public function localize_scripts() {
		wp_localize_script( $this->plugin_name . '-public-js', 'amstelodamum_ajax_vars', $this->get_ajax_vars() );
	}

	/**
	 * Get the AJAX data that WordPress needs to output.
	 *
	 * @since    0.2.0
	 *
	 * @return array
	 */
	private function get_ajax_vars() {
		return array(
			'nonce' => wp_create_nonce( $this->plugin_name . '-nonce' ),
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'ajax_action' => 'update_search_results',
			'ajax_repo_url' => $this->baseurl,
			'items_per_page' => $this->items_per_page,
		);
	}

	/**
	 * Query publications with option publication_id meta query (for results table)
	 *
	 * @since    0.2.0
	 */
	private function get_publications( $ids = array() ) {
		$args = array(
			'post_type' => 'publication',
			'posts_per_page' => -1,
			'meta_key' => 'publication_year',
			'orderby' => 'meta_value_num',
			'order' => 'ASC',
		);
		// If $pubids are presented, add metaquery
		if ( count( $ids ) > 0 ) {
			$args['meta_query'] = array(
				'key' => 'publication_id',
				'value' => $ids,
				'compare' => 'IN',
			);
		}
		$query = new WP_Query( $args );
		return $query->posts;
	}

	/**
	 * Build search form function
	 *
	 * @since    0.2.0
	 */
	private function build_search_form() {
		$helptexts = array(
			'desc' => __( 'Vul een zoekterm in.', 'amstelodamum-archive' )
				. ' <em>' . __( 'Tip: het is mogelijk om operators als AND, OR en NOT te gebruiken.', 'amstelodamum-archive' ) . '</em>',
			'loading' => __( 'Uw zoekopdracht wordt uitgevoerd.', 'amstelodamum-archive' ),
			'noresults' => __( 'Geen resultaten.', 'amstelodamum-archive' ),
			'short' => __( 'Deze zoekopdracht is te kort.', 'amstelodamum-archive' ),
			'error' => __( 'Er ging iets mis bij het verwerken van uw zoekopdracht.', 'amstelodamum-archive' ),
			'results' => __( 'Aantal gevonden publicaties:', 'amstelodamum-archive' ) . ' <span class="ams-search__help__results__hits"></span>',
			'truncated' => sprintf( __( 'Uw zoekopdracht heeft per publicatie méér treffers opgeleverd dan we kunnen weergeven. Probeer uw zoekopdracht te verfijnen.', 'amstelodamum-archive' ), $this->items_per_page ),
			'limit' => sprintf( __( 'Uw zoekopdracht heeft treffers opgeleverd in meer dan %s publicaties. Klik onderaan het onderzicht op de knop \'Volgende pagina\' om meer resultaten te zien.', 'amstelodamum-archive' ), $this->items_per_page ),
		);
		$output = '<form class="ams-search"><fieldset><label>Uw zoekopdracht:';
		$output .= '<input class="ams-search__input" name=\"ams_search_input" type="text"></label>';
		$output .= '<p class="ams_search__help">';
		foreach ( $helptexts as $el => $text ) {
			$hide = ( 'desc' !== $el ) ? ' style="display: none;"' : '';
			$output .= '<small class="ams-search__help__' . $el . '"' . $hide .'>' . $text . '</small>';
		}
		$output .= '</p>';
		$output .= '<button class="ams-search__submit">' . __( 'Zoeken', 'amstelodamum-archive' ) . '</button></fieldset></form>';
		$output .= '<div class="results-wrapper"></div>';
		return $output;
	}

		/**
		 * Get status from server
		 *
		 * @since    0.2.0
		 *
		 * @return status from json status response.
		 */

	private function get_repo_status() {
		$url = trailingslashit( get_option( $this->option_name . '_repo_url' ) ) . 'status';
		$get = wp_remote_get( $url );
		if ( is_wp_error( $get ) || empty( $get['body'] ) ) {
			$response = 'offline (repository is offline)';
		} else {
		    $response = json_decode( $get['body'] );
		}
		return $response;
	}

	/**
	 * Get status from server
	 *
	 * @since    0.3.0
	 *
	 * @return status from json status response.
	 */
	 private function get_page_by_template( $template ) {
		 $args = array(
	    'post_type' => 'page',
			'meta_key' => '_wp_page_template',
			'meta_value' => $template,
			'fields' => 'ids',
		);
		$template_query = new WP_Query( $args );
		if ( !empty( $template_query->posts[0] ) && is_int( $template_query->posts[0] ) ) {
			$page_id = $template_query->posts[0];
			return $page_id;
		} else {
			return false;
		}
	}

	/**
	 * Build search results table (with optional field for page numbers)
	 *
	 * @since    0.2.0
	 *
	 * @param array $query_results Array with search query results.
	 * @return string $output Stringy html for table.
	 */
	public function build_results_table( $query_results = false ) {
		$output = '';
		$found_pubs = array();
		$sum = 0;
		$table_header = '';
		$trunc_text = '';
		if ( $query_results ) {
			$hit_count = count( $query_results['docs'] );
			if ( $hit_count > 0 ) {
	
				foreach ( $query_results['docs'] as $doc ) {
					$found_pubs[ $doc['id'] ] = $doc['pages'];
					$page_length = count( $doc['pages'] );
					if ( $page_length > 0 ) {
						$sum += $page_length;
					}
				}
	
				if ( isset( $query_results['results_meta']['_truncated_to'] ) ) {
					$trunc_to = $query_results['results_meta']['_truncated_to'];
					$trunc_text =' (totaal aantal treffers is gebaseerd op ' . sprintf( _n( '%1$d publicatie', '%1$d publicaties', $trunc_to ), $trunc_to, 'amstelodamum-archive' ) . ')';
				}
	
				$table_header = '<header>' . __( 'Gevonden: ', 'amstelodamum-archive' )
					. sprintf( _n( '%1$d treffer', '%1$d treffers', $sum, 'amstelodamum-archive' ), $sum )
					. ' in ' . sprintf( _n( '%1$d publicatie', '%1$d publicaties', $query_results['results_meta']['_docs_total'] ), $query_results['results_meta']['_docs_total'], 'amstelodamum-archive' )
					. $trunc_text . '</header>';
	
			}
		}
		$ids = array_keys( $found_pubs );
		$results = $this->get_publications( $ids );
		if ( $results ) {
			$output .= $table_header;
			foreach ( $results as $pub ) {
				if ( empty( $pub->ID ) ) {
					continue;
				}
				$output .= $this->build_results_item( $pub, $found_pubs );
			}
			if ( $query_results['results_meta']['_docs_total'] > $this->items_per_page || isset( $trunc_to ) ) {
				$current_results_page = $query_results['results_meta']['_results_page'];
				$output .= $this->build_results_nav( $current_results_page, $query_results['results_meta'] );
			}
		} else {
			$output = '<p>'.__( 'Er zijn helaas geen publicaties gevonden', 'amstelodamum-archive' ) . '</p>';
		};
		return $output;
	}

	private function build_results_item( $pub, $found_pubs ) {
		$item = '';
		$post_id = $pub->ID;
		$order = get_field( 'publication_link', $post_id );
		if ( empty( $order ) ) {
			$id = get_field( 'publication_id', $post_id );
			$size = get_field( 'publication_filesize_mb', $post_id );
			$show = get_field( 'publication_in_repo', $post_id );
			if ( $show ) {
				$item .= '<table class="publication-wrapper">';
				$item .= '<tr>
								<td>
									<span class="publication-title">' . $pub->post_title .'</span>
									<small style="float: right; font-size: small;">
										<a href="'. $this->baseurl . 'resources/' . $id . '.pdf" target="_blank">Downloaden (' . $size .' MB)</a>
									</small>
								</td>
							</tr>';
			}
			if ( $show ) {
				if ( ! empty( $found_pubs ) && isset( $found_pubs[ $id ] ) && count( $found_pubs[ $id ] ) > 0 ) {
					$hit_list = array();
					$hit_output = '';
					foreach ( $found_pubs[ $id ] as $page ) {
						$hit_list[] = $page['page_number'];
						$hit_line_output = '';
						foreach ( $page['hit_lines'] as $hit_line ) {
							if ( empty( $hit_line ) ) {
								continue;
							}
							$hit_line_output .= '<small style="color: #777;">' . $hit_line . '</small><br />';
						}
						if ( empty( $hit_line_output ) ) {
							$hit_line_output = '<small style="color: #777;">' . __( ' ... ', 'amstelodamum-archive') . '</small><br />';
						}
						$hit_output .= '<li style="list-style: none; margin-bottom: 0.25em;"><small class="publication-page_no" style="color: #f24000; border: 1px solid #f24000; border-radius: 0.25em; padding: 0.25em; margin-right: 0.5em;">' . $page['page_number'] . '</small>' . $hit_line_output . '</li>';
					}
					$pagestring = implode( $hit_list, ', ' );
					$item .= '<tr class="publication-summary"><td>';
					$item .= '<small>' . __( "Treffer(s) op de volgende pagina('s): ", 'amstelodamum-archive' ). $pagestring . '</small>';
					$item .= '<small style="float: right;"><a class="details" href="#">Details &#x25BE;</a></small>';
					$item .= '<div class="publication-hit-lines" style="height:0; overflow: hidden;"><ul style="margin: 0;">';
					$item .= $hit_output;
					$item .= '</ul></div>';
					$item .= '</td></tr>';
				}
				$item .= '</table>';
			}
		}
		return $item;
	}

	private function build_results_nav( $current_results_page = 1, $results_meta ) {
		$start_from = 0;
		$query_attr = '';
		$next = $results_meta['_results_page'] + 1;
		$prev = $results_meta['_results_page'] - 1;
		if ( ! empty( $results_meta['_start_from'] ) ) {
			$start_from = intval( $results_meta['_start_from'] );
		}
		if ( isset( $results_meta['_truncated_to'] ) ) {
			$start_from_attr = ' data-start_from="' . strval( $start_from + intval( $results_meta['_truncated_to'] ) + 1 ) . '"';
		}
		$nav = '<div class="ams-paging-wrapper">';

		if ( $results_meta['_results_page'] > 1 ) {

			$nav .=  '<button class="button ams-paging__submit ams-paging__submit--previous" data-page="' . $prev . '" style="float: left;"">' . __( 'Vorige pagina', 'amstelodamum-archive' ) . '</button></p>';
		}
		if ( $results_meta['_docs_total'] - ( $results_meta['_results_page'] * $this->items_per_page ) > 0 || isset( $results_meta['_truncated_to'] ) ) {
			$nav .=  '<button class="button ams-paging__submit ams-paging__submit--next" data-page="' . $next . '"' . $start_from_attr . $query_attr . ' style="float: right;">' . __( 'Volgende pagina' , 'amstelodamum-archive' ) . '</button></p>';
		}
		$nav .= '</div>';
		return $nav;
	}

	/**
	 * Update results table with AJAX data.
	 *
	 * @return array
	 */
	public function update_search_results() {
		if ( isset( $_POST['json'] ) ) {
			$response = $_POST['json'];
			$response['results_meta'] = $_POST['results_meta'];
			// if ( ! isset( $response['_hits'] ) ) {
			// 	_e( 'Geen (geschikte) resultaten gevonden' , 'amstelodamum-archive' );
			// } else {
			echo $this->build_results_table( $response );
			//}
		} else {
			_e( 'Er is iets misgegaan met het verwerken van uw verzoek', 'amstelodamum-archive' );
		}
		wp_die();
	}

	/**
	 * Add original post content without the loop.
	 *
	 * @since    0.2.0
	 */
	public function build_content( $post_id ) {
		$output = '';
		$result = get_post( $post_id );
		if ( $result ) {
			$output .= apply_filters( 'the_content',$result->post_content );
		}
		echo $output;
	}

	public function add_shortcodes() {
		\add_shortcode('amstelodamum_archive_search', [$this,'build_repo_access']);
		\add_shortcode('amstelodamum_archive_overview', [$this,'build_results_table']);
	}

	/**
	 * Check repo status and if OK, build search form
	 *
	 * @since    0.2.0
	 */
	public function build_repo_access( $search = true ) {

		$response = $this->get_repo_status();
		if ( empty( $response->status ) || 'string' != gettype( $response->status ) ) {
			$this->repo_status = 'niet beschikbaar';
		}	else {
			$this->repo_status = $response->status;
		}
		// add extra link to overview (link generated from template )
		$link_msg = '';
		$status_wrapped = '<span class="ams-search__status-message">'. $this->repo_status . '</span>';
		$status_msg = sprintf( __( 'Het archief en de zoekfunctie zijn momenteel %s.', 'amstelodamum-archive' ), $status_wrapped );

		if ( 'online' === $this->repo_status ) {
			$output = '<p>' . $status_msg . ' ' . $link_msg . '</p>' . $this->build_search_form();
		}	elseif ( 'offline' === $this->repo_status ) {
			$output = '<p>' . $status_msg . $link_msg . '</p>' . $this->build_results_table();
		} elseif ( 'online' === $this->repo_status && $search === false ) {
			$output = $this->build_results_table();
		} else {
			$output = '<p>' . $status_msg . '</p>';
		}
		return $output;
	}

	/**
	 * Check repo status and if OK, build search form
	 *
	 * @since    0.2.0
	 */
	private function init() {
		$repo_response = $this->get_repo_status();
		if ( empty( $repo_response ) ) {
			$this->repo_status = 'offline (server is offline)';
		} else {
			if ( ! isset( $repo_response->status ) ) {
				$this->repo_status = 'offline (server is offline)';
			} else {
				$this->repo_status = $this->get_repo_status()->status;
			}
		}
	}
}
