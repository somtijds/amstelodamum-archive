(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write $ code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	 /**
	  * Define Ams_Ajax namespace.
	  */
	if (typeof Ams_Ajax == "undefined") {
		var Ams_Ajax = {
			'prevQuery': []
		};
	};

	Ams_Ajax.stripTrailingSlash = function( url ) {
		return url.replace(/\/$/, "");
	}

	Ams_Ajax.showHelpText = function( msg, hits ) {
		/* hide all help texts */
		$( '.form-wrapper' ).find('small').each(function() {
			$(this).hide();
		});

		if ( typeof hits === 'undefined' ) {
			hits = 0;
		}
		if ( 'results' === msg && amstelodamum_ajax_vars.items_per_page >= hits ) {
			$( '.ams-search__help__results__hits' ).html( hits.toString() + ' ' );
		}
		/* disable button while loading */
		if ( 'loading' === msg ) {
			$( '.ams-search__submit,.ams-paging__submit').attr('disabled','disabled');

		} else {
			$( '.ams-search__submit,.ams-paging__submit').removeAttr('disabled');
		}
		/* show the right one */
		$( '.form-wrapper small.ams-search__help__' + msg ).show();
	};

	/* Function to animate height: auto */
	Ams_Ajax.animateDetails = function(element, time) {
		var curHeight = element.height(), // Get Default Height
		autoHeight = element.css('height', 'auto').height(); // Get Auto Height
		element.height(curHeight); // Reset to Default Height
		element.stop().animate({ height: autoHeight }, time); // Animate to Auto Height
	};

	Ams_Ajax.addResultsTableTriggers = function() {
		var detailLinks = $('a.details');
		detailLinks.each(function() {
			$( this ).on('click',function( e ){
				e.preventDefault();
				var animateTime = 500,
				detailWrapper = $( this ).parent('small').siblings('.publication-hit-lines');
				if(detailWrapper.height() === 0){
					$( this ).html('Details &#x25B4;');
					Ams_Ajax.animateDetails(detailWrapper, animateTime);
				} else {
					$( this ).html('Details &#x25BE;');
					detailWrapper.stop().animate({ height: '0' }, animateTime);
				}
			})
		})
		$( 'button.ams-paging__submit' ).on( 'click', function( e ) {
			e.preventDefault();
			var goToPage = parseInt( $(this).data('page') ),
			startFrom = 0;
			if ( $(this).data('start_from') !== undefined ) {
				startFrom = parseInt( $(this).data('start_from') );
			} else {
				var queryString = Ams_Ajax.isStoredQuery( goToPage );
				if ( queryString ) {
					startFrom = parseInt ( Ams_Ajax.getQueryVariable( queryString,'from') );
				}
			}
			Ams_Ajax.queryRepo( goToPage, startFrom );
		})
	};

	Ams_Ajax.sortPublications = function(a,b) {
		if (a.id < b.id) {
			return -1;
		}
		if (a.id > b.id) {
			return 1;
		}
		return 0;
	};

	Ams_Ajax.updateResultsTable = function( results, results_meta ) {
		var data = {
			'nonce' : amstelodamum_ajax_vars.nonce,
			'action' : amstelodamum_ajax_vars.ajax_action,
			'results_meta' : results_meta,
			'json' : results,
		};
		var request = $.ajax( {
			'url' : Ams_Ajax.stripTrailingSlash( amstelodamum_ajax_vars.ajax_url ),
			'method' : 'POST',
			'data' : data
		});
		request.done( function( response ) {
			$( '.results-wrapper' ).html( response );
			Ams_Ajax.addResultsTableTriggers();
		})
		.fail( function( error ) {
			Ams_Ajax.showHelpText( 'error' );
			console.log( error );
		})
	};

	/**
	 * Reduce results to 150 hits to be sent to AJAX-function
	 *
	 * @TODO: Make this an option / AJAX-variable
	 */
	Ams_Ajax.returnBatch = function( results ) {
		var selection = {
			'docs': []
		},
		sum = 0,
		i = 0;
		/* Disabling sorting, this is done in Elastic already
		results.docs.sort(Ams_Ajax.sortPublications);*/
		while( sum < 150 && i < results.docs.length ) {
			selection.docs.push(results.docs[i]);
			sum += results.docs[i].pages.length;
			i++;
		}
		selection._docs = i;
		selection._sum = sum;
		if ( selection._docs < results._docs ) {
			selection._truncated_from = results._docs;
			selection._truncated_to = selection._docs;
		}
		return selection;
	};

	/**
	 * Get query variables to insert into next query
	 *
	 * @TODO: this is now only used by the paging query variable, could be expanded.
	 */
	Ams_Ajax.getQueryVariable = function(query,variable) {
		var queryvars = query.split('?');
		if ( variable === 'query' ) {
			return decodeURIComponent(queryvars[0]);
		}
		var vars = queryvars[1].split('&');
		for (var i = 0; i < vars.length; i++) {
			var pair = vars[i].split('=');
			if (decodeURIComponent(pair[0]) == variable) {
				return decodeURIComponent(pair[1]);
			}
		}
	};

	Ams_Ajax.isStoredQuery = function(pagingNumber,queryString) {
		var index = pagingNumber - 1;
		var storedQuery = Ams_Ajax.prevQuery[index];
		if ( storedQuery !== undefined ) {
			if ( queryString !== undefined && queryString !== Ams_Ajax.getQueryVariable(storedQuery,'query') ) {
				/* empty prevQuery array if the queryString is different than the one stored in the prevQuery variable */
				console.log( 'erasing prevQuery, wrong querystring given' );
				Ams_Ajax.prevQuery = [];
			}
			return storedQuery;
		} else {
			console.log( 'query for page ' + pagingNumber + ' does not exist:' + queryString );
			return false;
		}
	}

	Ams_Ajax.getQueryData = function( pagingNumber, startFrom ) {
		console.log( 'pagingNumber :' + pagingNumber );
		var queryData = {};
		if ( pagingNumber === undefined ) {
			var pagingNumber = 1;
		} else {
			pagingNumber = parseInt( pagingNumber);
		}
		var queryString = $('input.ams-search__input').val();
		if ( queryString.length < 3 ) {
			Ams_Ajax.showHelpText( 'short' );
			return;
		}
		/* the old query becomes the query */
		var storedQuery = Ams_Ajax.isStoredQuery(pagingNumber,queryString)
		if ( storedQuery ) {
			console.log( storedQuery );
			queryData.string = storedQuery;
			queryData.from = Ams_Ajax.getQueryVariable(storedQuery,'from');
		} else {
			if ( typeof pagingNumber !== 'number' || pagingNumber < 1 ) {
				return;
			}
			if ( pagingNumber === 1 ) {
				queryData.from = 0;
			}
			if ( typeof pagingNumber === 'number' && pagingNumber > 1 ) {
				if ( startFrom !== undefined && startFrom !== 0 ) {
					queryData.from = parseInt( startFrom );
				} else {
					console.log( 'figuring out what the startFrom should be if it cannot be 0 and is not set either' );
					queryData.from = ( pagingNumber - 1 ) * parseInt( amstelodamum_ajax_vars.items_per_page );
				}
			}
			var queryVariableString = encodeURIComponent(queryString) + '?page=' + pagingNumber + '&size=' + amstelodamum_ajax_vars.items_per_page + '&from=' + queryData.from;
			queryData.string = queryVariableString;
		}
		return queryData;
	};

	/**
	 * Process query results to PHP-script that returns HTML
	 *
	 * @TODO reduce to one query and use browser memory / the URL for the previous button
	 */
	Ams_Ajax.queryRepo = function( pagingNumber, startFrom ) {
		var queryData = Ams_Ajax.getQueryData( pagingNumber, startFrom );
		console.log(queryData);
		if ( typeof queryData.string !== 'string' ) {
			return;
		}
		Ams_Ajax.showHelpText( 'loading' );
		var request = $.ajax( {
			'url': amstelodamum_ajax_vars.ajax_repo_url + 'search/' + queryData.string,
			'dataType': 'jsonp',
			'timeout': 10000,
			'beforeSend': function() {
				Ams_Ajax.showHelpText( 'loading' );
			}
		});
		request.done( function( results ) {
			Ams_Ajax.prevQuery.push( queryData.string );
			if ( typeof results._docs === 'number' ) {
				var	results_meta = {
					'_docs' : results._docs,
					'_docs_total' : results._docs_total,
					'_query' : queryData.string,
					'_page_size' : parseInt( amstelodamum_ajax_vars.items_per_page ),
					'_results_page' : parseInt( Ams_Ajax.getQueryVariable(queryData.string,'page') ),
					'_start_from' : queryData.from
				};
				if ( amstelodamum_ajax_vars.items_per_page > results._docs_total ) {
					Ams_Ajax.showHelpText( 'results', results._docs );
				} else if ( amstelodamum_ajax_vars.items_per_page < results._docs_total ) {
					Ams_Ajax.showHelpText( 'limit', results._docs_total );
				}
				results = Ams_Ajax.returnBatch( results );
				if ( results._truncated_from !== undefined ) {
					Ams_Ajax.showHelpText( 'truncated' );
					results_meta['_truncated_from'] = results._truncated_from;
					results_meta['_truncated_to'] = results._truncated_to;
				}
				console.log( results_meta );
				Ams_Ajax.updateResultsTable( results, results_meta );
			} else {
				Ams_Ajax.showHelpText( 'noresults' );
			}
		})
		.fail( function( error ) {
			Ams_Ajax.showHelpText( 'error' );
			console.log( error );
		})
	};

	$( document ).ready( function() {
		//console.log( amstelodamum_ajax_vars );
		$( 'button.ams-search__submit' ).on( 'click', function( e ) {
			e.preventDefault();
			/* empty prevQuery array if the queryString is different than the one stored in the prevQuery variable */
			Ams_Ajax.prevQuery = [];
			Ams_Ajax.queryRepo();
		})
		$( 'input.ams-search__input' ).on( 'focus', function( ) {
			Ams_Ajax.showHelpText( 'desc' );
		})
	})

})( jQuery );
