<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.somtijds.nl
 * @since      0.1.0
 *
 * @package    Amstelodamum_Archive
 * @subpackage Amstelodamum_Archive/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Amstelodamum_Archive
 * @subpackage Amstelodamum_Archive/admin
 * @author     Willem Prins <willem@somtijds.nl>
 */
class Amstelodamum_Archive_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The options name to be used in this plugin
	 *
	 * @since  	0.1.0
	 * @access 	private
	 * @var  	string 		$option_name 	Option name of this plugin
	 */
	private $option_name = 'archive_settings';

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_styles() {

		/**
		 * An instance of this class should be passed to the run() function
		 * defined in Amstelodamum_Archive_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Amstelodamum_Archive_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/amstelodamum-archive-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_scripts() {

		/**
		 * An instance of this class should be passed to the run() function
		 * defined in Amstelodamum_Archive_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Amstelodamum_Archive_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/amstelodamum-archive-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Add an options page under the Settings submenu
	 *
	 * @since  0.1.0
	 */
	public function add_options_page() {

		$this->plugin_screen_hook_suffix = add_options_page(
			__( 'Archief Instellingen', 'amstelodamum-archive' ),
			__( 'Amstelodamum Archief', 'amstelodamum-archive' ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'display_options_page' )
		);

	}

	/**
	 * Render the options page for plugin
	 *
	 * @since  0.1.0
	 */
	public function display_options_page() {
		include_once 'partials/amstelodamum-archive-admin-display.php';
	}

	/**
	 * Register all related settings for the plugin
	 *
	 * @since     0.1.0
	 * @return    boolean    The version number of the plugin.
	 */
	public function register_setting() {

		// Add a General section
		add_settings_section(
			$this->option_name . '_general',
			__( 'Algemeen', 'amstelodamum-archive' ),
			array( $this, $this->option_name . '_general_cb' ),
			$this->plugin_name
		);

		add_settings_field(
			$this->option_name . '_repo_url',
			__( 'Servergegevens', 'amstelodamum-archive' ),
			array( $this, $this->option_name . '_repo_url_cb' ),
			$this->plugin_name,
			$this->option_name . '_general',
			array( 'label_for' => $this->option_name . '_repo_url' )
		);

		register_setting( $this->plugin_name, $this->option_name . '_repo_url', array( $this, $this->option_name . '_sanitize_repo_url' ) );

	}


		/**
	 * Render the text for the general section
	 *
	 * @since  0.1.0
	 */
	public function archive_settings_general_cb() {
		echo '<p>' . __( 'Om de gebruikers van de site toegang te verschaffen tot tot digitale archief van Amstelodamum, moeten hieronder een aantal basisgegevens worden ingevuld.', 'amstelodamum-archive' ) . '</p>';
	}

	 /**
	 * Render the radio input field for position option
	 *
	 * @since  0.1.0
	 */
	public function archive_settings_repo_url_cb() {
		$url = get_option( $this->option_name . '_repo_url' );
		?>
			<fieldset>
				<label for="<?php echo $this->option_name .'_repo_url' ?>">
					<?php _e( 'Repository URL', 'amstelodamum-archive' ); ?>
				</label>
				<input type="text"
							 name="<?php echo $this->option_name . '_repo_url' ?>"
							 id="<?php echo $this->option_name . '_repo_url' ?>"
							 placeholder="<?php _e( 'Vul hier een geldige URL in, zoals https://archief.amstelodamum.nl/publications/','amstelodamum-archive' ); ?>"
							 <?php if ( !empty( $url ) ) : ?>
								 value="<?php echo $url; ?>">
							 <?php else: ?>
								 >
							 <?php endif; ?>
			</fieldset>
		<?php
	}

	/**
	 * Sanitize the url value before being saved to database
	 *
	 * @param  string $position $_POST value
	 * @since  0.1.0
	 * @return string           Sanitized value
	 */
	public function archive_settings_sanitize_repo_url( $url ) {
		return $url;
	}

	/**
	 * Get option name from admin class
	 *
	 * @param  string $position $_POST value
	 * @since  0.2.0
	 * @return string           Sanitized value
	 */
	public function get_option_name() {
		return $this->option_name;
	}

}
