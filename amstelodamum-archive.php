<?php
/**
 * Deze plugin is verantwoordelijk voor de koppeling met het digitale archief op de website van Genootschap Amstelodamum
 *
 * @link              http://www.somtijds.nl
 * @since             0.1.0
 * @package           Amstelodamum_Archive
 *
 * @wordpress-plugin
 * Plugin Name:       Amstelodamum Archief
 * Plugin URI:        https://bitbucket.org/retrorism/amstelodamum-archive
 * Description:       Deze plugin maakt het mogelijk om te zoeken op het digitale archief op de website van Genootschap Amstelodamum
 * Version:           0.2.0
 * Author:            Willem Prins
 * Author URI:        http://www.somtijds.nl
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       amstelodamum-archive
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-amstelodamum-archive-activator.php
 */
function activate_amstelodamum_archive() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-amstelodamum-archive-activator.php';
	Amstelodamum_Archive_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-amstelodamum-archive-deactivator.php
 */
function deactivate_amstelodamum_archive() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-amstelodamum-archive-deactivator.php';
	Amstelodamum_Archive_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_amstelodamum_archive' );
register_deactivation_hook( __FILE__, 'deactivate_amstelodamum_archive' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-amstelodamum-archive.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.1.0
 */
function run_amstelodamum_archive() {

	$plugin = new Amstelodamum_Archive();
	$plugin->run();

}
run_amstelodamum_archive();
