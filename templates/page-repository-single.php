<?php
/**
 * Template Name: Archief enkele publicatie
 * A template used to demonstrate how to include the template
 * using this plugin.
 *
 * @package Amstelodamum_Archive
 * @since 	0.1.0
 * @version	0.1.0
 */

 get_header(); ?>

 	<div id="primary" class="content-area">
 		<main id="main" class="site-main" role="main">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header><!-- .entry-header -->

				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>
 		</main><!-- .site-main -->

 		<?php get_sidebar( 'content-bottom' ); ?>

 	</div><!-- .content-area -->

 <?php get_sidebar(); ?>
 <?php get_footer(); ?>
